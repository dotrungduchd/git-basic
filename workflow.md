### Basic workflow

#### Branch

- main: default branch (reflects a production-ready state)
- feature: checkout from main, work on new features
- hoxfix: checkout from main, fix on production
- release
  - development: development environment
  - production: production environment

#### Workflow

- dev clone source code from repository (gitlab, github, bitbucket...)
- **dev create new branch (feature)**
- dev work on new branch
- **dev merge new branch into main branch**
- **dev merge main branch into development branch (release/development)**
- dev deploy and QC test development environment
- **dev merge main/development branch into production branch (release/production)**

Example:

```
git clone https://github.com/example_repository
```

- **dev create new branch (feature)**

```
git checkout -b feature/naming
```

- **coding and push to origin new branch**

```
- git add
- git commit -m "message"
- git push --set-upstream origin feature/naming
```

- **dev merge new branch into main branch**

```
- git checkout main
- git merge feature/naming
- git push
```

- **dev merge main branch into development branch (release/development)**

```
- git checkout release/development
- git merge main
- git push
```

- **dev merge main/development branch into production branch (release/production)**

```
- git checkout release/production
- git merge main (git merge release/development)
- git push
```
